-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Inang: 127.0.0.1
-- Waktu pembuatan: 04 Feb 2015 pada 01.21
-- Versi Server: 5.5.27
-- Versi PHP: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Basis data: `db_atm`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `nasabah`
--

CREATE TABLE IF NOT EXISTS `nasabah` (
  `Norek` varchar(20) NOT NULL,
  `PIN` varchar(6) DEFAULT NULL,
  `Nama` varchar(50) NOT NULL,
  `Alamat` varchar(100) NOT NULL,
  `Saldo` int(11) NOT NULL,
  PRIMARY KEY (`Norek`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nasabah`
--

INSERT INTO `nasabah` (`Norek`, `PIN`, `Nama`, `Alamat`, `Saldo`) VALUES
('0028289829', '829238', 'haris', 'pga', 750000),
('0038288178', '989899', 'rimba', 'malang', 900000),
('008942395', '412411', 'dimas', 'pga', 900000),
('0096255354', '241414', 'ardi', 'pga', 800000),
('01189492', '421411', 'maman sutarna', 'bandung', 840000),
('013331232', '313211', 'kakan', 'sulawesi', 8000100),
('0144211144', '625546', 'udin', 'sukabirus', 400000),
('01682419', '412414', 'tiara', 'pagarsih', 200000),
('019277484', '242414', 'didin', 'malang', 4790000),
('019424244', '523532', 'lala', 'lulu', 940000),
('020935935', '214141', 'bagas', 'pga', 82000000),
('022414121', '423442', 'sutisna', 'bandung', 31650000),
('023878788', '241414', 'nanda', 'pekalongan', 42040400),
('026818108', '742750', 'uknown', 'bojongloa', 590000000),
('02687417', '874817', 'papah', 'tegalsawah', 852000000),
('0288281', '213799', 'kumil', 'pgA', 92010000),
('02889u2509', '352352', 'mamul', 'kalongan', 20200000),
('03028390', '293202', 'kakanda', 'madira', 8000000),
('12345678', '132435', 'Percobaan', 'coba coba', 1000000),
('6306120011', '098765', 'jajang', 'demang', 69000),
('6306120018', '233456', 'Abdillah Israqi Zihni', 'Depok', 930000),
('630612009', '723472', 'Kurnia Bintoro', 'Palembang', 900000),
('6306124110', '123456', 'Kahfi Gunardi', 'Gang demang sukabirus', 1990000),
('73664772', '412515', 'Siti ratmanah', 'tegalsawah', 70700000),
('73667485', '937747', 'Fitriani', 'Tegalsawah', 99000000),
('938747495', '038374', 'Cantra', 'Tegalsawah', 100000000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pembrs`
--

CREATE TABLE IF NOT EXISTS `pembrs` (
  `id_pembayaran` varchar(50) NOT NULL DEFAULT '',
  `jenis` varchar(50) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `jml_tagihan` int(11) NOT NULL,
  `tgl` varchar(20) NOT NULL,
  PRIMARY KEY (`id_pembayaran`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pembrs`
--

INSERT INTO `pembrs` (`id_pembayaran`, `jenis`, `nama`, `jml_tagihan`, `tgl`) VALUES
('09876543212', 'Rawat Inap', 'sutisna', 1000000, '12-02-2014'),
('110019918829', 'Rawat Inap', 'jajang', 400000, '12-02-2014'),
('12345678909', 'Premi', 'sutisna', 1000000, '30-12-1013'),
('8098088888', 'Premi', 'sutisna', 450000, '30-11-1013'),
('8098089999', 'Premi', 'sutisna', 450000, '30-11-1013');

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi`
--

CREATE TABLE IF NOT EXISTS `transaksi` (
  `Norek` varchar(20) DEFAULT NULL,
  `Nama` varchar(50) NOT NULL,
  `Jenis` varchar(100) NOT NULL,
  `Tgl_Transaksi` varchar(20) NOT NULL,
  `Jumlah` int(11) NOT NULL,
  `Saldo_Akhir` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `transaksi`
--

INSERT INTO `transaksi` (`Norek`, `Nama`, `Jenis`, `Tgl_Transaksi`, `Jumlah`, `Saldo_Akhir`) VALUES
('12345678', 'Percobaan', 'Tarik Tunai', '2014-05-04', 100000, 2400000),
('12345678', 'Percobaan', 'Tarik Tunai', '2014-05-04', 1000000, 1400000),
('12345678', 'Percobaan', 'Tarik Tunai', '2014-05-04', 100000, 1300000),
('12345678', 'Percobaan', 'Tarik Tunai', '2014-05-04', 500000, 800000),
('6306124110', 'Kahfi Gunardi', 'Tarik Tunai', '2014-05-04', 1500000, 3700000),
('6306124110', 'Kahfi Gunardi', 'Tarik Tunai', '2014-05-04', 100000, 3600000),
('12345678', 'Percobaan', 'Tarik Tunai', '2014-05-04', 500000, 300000),
('6306124110', 'Kahfi Gunardi', 'Tarik Tunai', '2014-05-05', 100000, 3500000),
('6306124110', 'Kahfi Gunardi', 'Tarik Tunai', '2014-05-07', 100000, 3400000),
('6306124110', 'Kahfi Gunardi', 'Transfer ke norek 6306120018', '2014-05-07', 100000, 3100000),
('6306124110', 'Kahfi Gunardi', 'Transfer ke norek 6306120018', '2014-05-07', 50000, 3050000),
('6306124110', 'Kahfi Gunardi', 'Transfer ke norek 6306120018', '2014-05-07', 20000, 3030000),
('6306124110', 'Kahfi Gunardi', 'Transfer ke norek 6306120018', '2014-05-07', 30000, 3000000),
('6306124110', 'Kahfi Gunardi', 'Transfer ke norek 6306120018', '2014-05-07', 10000, 2990000),
('6306124110', 'Kahfi Gunardi', 'Transfer ke norek 6306120018', '2014-05-07', 10000, 2980000),
('6306124110', 'Kahfi Gunardi', 'Transfer ke norek 6306120018', '2014-05-07', 10000, 2970000),
('6306124110', 'Kahfi Gunardi', 'Transfer ke norek 73664772', '2014-05-07', 700000, 2270000),
('6306124110', 'Kahfi Gunardi', 'Beli Pulsa', '2014-05-07', 70000, 2200000),
('6306124110', 'Kahfi Gunardi', 'Beli Pulsa', '2014-05-07', 10000, 2190000),
('022414121', 'sutisna', 'Tagihan Rumah Sakit', '2014-05-07', 450000, 34450000),
('022414121', 'sutisna', 'Tagihan Rumah Sakit', '2014-05-07', 400000, 34050000),
('022414121', 'sutisna', 'Tagihan Rumah Sakit', '2014-05-08', 400000, 33650000),
('022414121', 'sutisna', 'Tagihan Rumah Sakit', '2014-05-08', 1000000, 32650000),
('022414121', 'sutisna', 'Polis Asuransi', '2014-05-08', 1000000, 31650000),
('0028289829', 'haris', 'Transfer ke norek 019277484', '2014-05-08', 50000, 750000),
('6306124110', 'Kahfi Gunardi', 'Tarik Tunai', '2014-05-09', 100000, 2090000),
('6306124110', 'Kahfi Gunardi', 'Tarik Tunai', '2014-05-09', 100000, 1990000);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
