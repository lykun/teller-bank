/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tellerbank;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author KAHFI
 */
public class Nasabah {
    Connection con=null;
    private String ID;
    private String PIN;
    private String nama;
    private String alamat;
    private int Saldo;
    private ArrayList<Transaksi> daftarTransaksi;

    public Nasabah() {
        daftarTransaksi = new ArrayList<>();
    }

    public Nasabah(String ID, String PIN, String nama, String alamat, int Saldo) {
        this.ID = ID;
        this.PIN = PIN;
        this.nama = nama;
        this.alamat = alamat;
        this.Saldo = Saldo;
        this.daftarTransaksi = new ArrayList<>();
    }
    
    public void addTransaksi(String nama, String idTransaksi)
    {
        Transaksi t = new Transaksi(nama, idTransaksi);
        daftarTransaksi.add(t);
    }

    public ArrayList<Transaksi> getDaftarTransaksi() {
        return daftarTransaksi;
    }
    
    
    public String getID() {
        return ID;
    }

    public String getPIN() {
        return PIN;
    }

    public String getNama() {
        return nama;
    }

    public String getAlamat() {
        return alamat;
    }

       public void setID(String ID) {
        this.ID = ID;
    }

    public void setPIN(String PIN) {
        this.PIN = PIN;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public void setSaldo(int Saldo) {
        this.Saldo = Saldo;
    }

    public int getSaldo() {
        return Saldo;
    }

    public void tambahnasabah(String ID,String PIN,String nama,String alamat,int Saldo){
        Database db = new Database();
        db.quey("insert into nasabah values('"+ID+"','"+PIN+"','"+nama+"','"+alamat+"','"+Saldo+"');");
    }
    public void deleteNasabah(String delete){
        Database db = new Database();
        db.quey("delete from nasabah where Norek='"+delete+"';");
    }
    public ResultSet getNasabah(String cari){
        ResultSet rs=null;
        String query="select * from nasabah where Norek like '%"+cari+"%';";
        rs=new Database().getData(query);
        return rs;
    }
    public void updateNasabah(String ID,String PIN,String nama,String alamat,int Saldo){
         Database db = new Database();
         db.quey("update nasabah set PIN='"+PIN+"',Nama='"+nama+"',Alamat='"+alamat+"',Saldo='"+Saldo+"' where Norek='"+ID+"';");
    }
}
